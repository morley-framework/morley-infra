# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

{ pkgs, system }:

let
  scriptsdir = ./scripts;
  internal-modules-check-app = { hs-pkgs, projectSrc }:
    let env = hs-pkgs.ghcWithPackages (
          ps: with ps; [ filepath directory ghc-lib-parser ]
        );
        script = pkgs.writeShellScript "run-module-check" ''
          export PATH="${pkgs.lib.makeBinPath [ env ]}"
          exec ${env.targetPrefix}runghc \
            --ghc-arg=-package --ghc-arg=ghc-lib-parser \
            ${scriptsdir}/internal-module-check.hs ${projectSrc}
        '';
    in {
      type = "app";
      program = ''${script}'';
    };
  weeder = {
    collect-hie = ''
      mkdir $out/hie
      cd dist
      find . -type f -name '*.hie' -print0 | ${pkgs.cpio}/bin/cpio --pass-through --null --make-directories $out/hie
    '';
    app = { projectSrc, hs-pkgs, packages }: {
      type = "app";
      program = let
        pkg = pkgs.haskell-nix.hackage-package {
          name = "weeder";
          version = "2.5.0";
          compiler-nix-name =
            with hs-pkgs.ghc.identifier;
            builtins.replaceStrings ["."] [""] (name + version);
        };
        directories = with pkgs.lib; pipe packages [
          (mapAttrs collectPackagePaths)
          attrValues
          concatLists
          (builtins.map (x: ''--hie-directory "${x}"''))
          (concatStringsSep " ")
        ];
        collectPackagePaths = _: pkg: with pkgs.lib; concatLists [
          (optional (pkg ? library) pkg.library)
          (attrValues pkg.tests)
          (attrValues pkg.exes)
          (attrValues pkg.benchmarks)
        ];
        script = pkgs.writeShellScript "run-weeder" ''
            ${scriptsdir}/gen-weeder-conf.sh "${projectSrc}"
            exec ${pkg.components.exes.weeder}/bin/weeder ${directories}
          '';
        in ''${script}'';
    };
  };
  # TODO [#694]: consider building 'cabal-docspec' from source
  cabal-docspec = pkgs.stdenv.mkDerivation rec {
    name = "cabal-docspec";
    src = builtins.fetchurl {
      url = "https://github.com/phadej/cabal-extras/releases/download/cabal-docspec-0.0.0.20230517/cabal-docspec-0.0.0.20230517-x86_64-linux.xz";
      sha256 = "sha256:1mqysanbdd87h8ksnk30lr4fqql5c94xn0y1pcd6fkddcgjbnc9v";
    };
    phases = [ "installPhase" ];
    installPhase = ''
      mkdir -p $out/bin
      xz -d < ${src} > $out/bin/cabal-docspec
      chmod +x $out/bin/cabal-docspec
    '';
  };
  run-doctest-apps = { hs-pkgs, packages }:
    let mkRunDoctest = name: pkg:
      let
        env = hs-pkgs.ghcWithPackages (_: [ pkg.library ]);
        script = pkgs.writeShellScript "run-doctests" ''
          export PATH=${pkgs.lib.makeBinPath [ cabal-docspec env ]}
          ${env.targetPrefix}runghc \
            ${scriptsdir}/run-doctests.hs --ci ${name} -- \
              -w ${env.targetPrefix}ghc \
              --ignore-trailing-space
        '';
      in {
        name = "ci:doctest-${name}";
        value = { type = "app"; program = "${script}"; };
      };
    in pkgs.lib.mapAttrs' mkRunDoctest packages;
in {
  collect-hie = release: attrs: if release
    then attrs
    else pkgs.lib.recursiveUpdate attrs {
      ghcOptions = (attrs.ghcOptions or []) ++ ["-fwrite-ide-info"];
      postInstall = weeder.collect-hie;
    };
  apps = { projectSrc, hs-pkgs, local-packages }:
    let pnames = map (p: p.name) local-packages;
        packages = pkgs.lib.genAttrs pnames (packageName: hs-pkgs."${packageName}".components);
    in {
      "ci:internal-modules-check" = internal-modules-check-app { inherit hs-pkgs projectSrc; };
      "ci:weeder" = weeder.app { inherit hs-pkgs projectSrc packages; };
    } // (run-doctest-apps { inherit hs-pkgs packages; });
}
