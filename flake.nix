# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

{
  description = "The morley-infra flake";

  nixConfig.flake-registry = ./flake-registry.json;

  inputs = {
    xrefcheck.flake = false;
    stackage-nix.flake = false;
    haskell-nix.inputs.stackage.follows = "stackage-nix";
    hackage-nix.flake = false;
    haskell-nix.inputs.hackage.follows = "hackage-nix";
  };

  outputs = inputs@{ self, nixpkgs, flake-utils, xrefcheck, tezos-packaging, haskell-nix, serokell-nix, ... }:
    flake-utils.lib.eachSystem [
      "x86_64-linux"
      "x86_64-darwin"
    ] (system:
    let
      pkgs = haskell-nix.legacyPackages.${system}.extend serokell-nix.overlay;

      pkgs-legacy = nixpkgs.legacyPackages.${system};

      inherit (tezos-packaging.packages.${system}) octez-client;
    in {

      legacyPackages = pkgs.extend (_: _: {
        # TODO add as flake output after `xrefcheck` flakyfication
        # workaround, because it refuses to evaluate in pure mode
        xrefcheck = import inputs.xrefcheck {};

        inherit octez-client;

        inherit (self.packages.${system}) stack2cabal;
      });

      checks = {
        trailing-whitespace = pkgs.build.checkTrailingWhitespace ./.;
        reuse-lint = pkgs.build.reuseLint ./.;
      };

      # stack2cabal is broken because of strict constraints, set 'jailbreak' to ignore them
      packages.stack2cabal = pkgs-legacy.haskell.lib.overrideCabal pkgs-legacy.haskellPackages.stack2cabal (drv: {
        jailbreak = true;
        broken = false;
      });

      utils = {
        # Script that runs given network scenario with a dedicated moneybag in
        # isolated octez-client environment. Scenario to run should be defined
        # by the 'scenario' argument. 'TASTY_CLEVELAND_MONEYBAG_SECRET_KEY'
        # address will be refilled before the scenario is run (to have a balance
        # defined by the 'refill-balance' argument) and emptied after it is
        # completed (to avoid getting too many funds stuck).
        #
        # Usage example:
        # nix-build ci.nix -A <test-suite>
        # $(nix-build ci.nix -A run-chain-tests
        #   --argstr refill-balance 10
        #   --argstr node-endpoint "$TASTY_CLEVELAND_NODE_ENDPOINT"
        #   --argstr step-moneybag "$STEP_MONEYBAG"
        #   --argstr scenario './result/bin/<test-suite> --color=always --cleveland-mode=only-network' --no-out-link)
        #
        # In case step-moneybag key is password protected, use '--argstr
        # step-moneybag-password <password>'.
        run-chain-tests = { refill-balance ? "10", node-endpoint, step-moneybag, step-moneybag-password ? "", scenario }: let
          octez-client-bin="${octez-client}/bin/octez-client -d \"$TASTY_CLEVELAND_DATA_DIR\" -E ${node-endpoint}";
          password-arg = if step-moneybag-password == "" then "" else "<<< ${step-moneybag-password}";
        in pkgs.writeShellScript "run-chain-tests.sh" ''
          min_block_delay="$(${pkgs.curl}/bin/curl -s "${node-endpoint}/chains/main/blocks/head/context/constants" | ${pkgs.jq}/bin/jq -r .minimal_block_delay)"
          get_curr_level="${pkgs.curl}/bin/curl -s "${node-endpoint}/chains/main/blocks/head/metadata" | ${pkgs.jq}/bin/jq -r .level_info.level"
          wait_next_block() {
            start_level="$(eval $get_curr_level)"
            while [[ "$start_level" == "$(eval $get_curr_level)" ]]; do
              sleep "$min_block_delay"
            done
          }

          TASTY_CLEVELAND_DATA_DIR="$(mktemp -d --tmpdir="$PWD")"
          export TASTY_CLEVELAND_DATA_DIR
          export PATH=${octez-client}/bin:$PATH

          ${octez-client-bin} import secret key step-moneybag ${step-moneybag} ${password-arg}
          ${octez-client-bin} gen keys moneybag --force
          TASTY_CLEVELAND_MONEYBAG_SECRET_KEY="$(${octez-client-bin} show address moneybag -S | grep "Secret Key:" | cut -d' ' -f3)"
          export TASTY_CLEVELAND_MONEYBAG_SECRET_KEY
          # Transfer may fail due to a race condition in case multiple CI steps are running simultaneously,
          # so we retry it until it succeeds.
          until ${octez-client-bin} transfer ${refill-balance} from step-moneybag to moneybag --burn-cap 0.1 ${password-arg}; do
            wait_next_block
          done
          ${scenario}
          scenario_exit_code="$?"
          # Wait one block to avoid race conditions with the scenario itself, in the
          # unlikely case that the moneybag just did a manager operation there.
          wait_next_block
          # Transfer all remaining balance (minus 0.1 to account for fees) back, to
          # avoid too many funds getting stuck.
          # Note: no retry loop here because no other job should be using this moneybag
          remaining_amount="$(${octez-client-bin} get balance for moneybag | cut -d' ' -f 1)"
          transfer_amount="$(echo "$remaining_amount-0.1" | ${pkgs.bc}/bin/bc -l)"
          ${octez-client-bin} transfer "$transfer_amount" from moneybag to step-moneybag
          exit "$scenario_exit_code"
        '';

        ci-apps = import ./ci-apps.nix { inherit pkgs system; };

        # checks if all packages are appropriate for uploading to hackage
        run-cabal-check = { local-packages,  projectSrc }:
          pkgs.runCommand "cabal-check" { buildInputs = [ pkgs.cabal-install ]; } ''
            ${pkgs.lib.concatMapStringsSep "\n" ({ name, subdirectory }: ''
              echo 'Running `cabal check` for ${name}'
              cd ${projectSrc}/${subdirectory}
              cabal check
            '') local-packages}

            touch $out
          '';

        # GHC has issues with compiling statically linked executables when Template Haskell
        # is used, this wrapper for ld fixes it. Another solution we could use is to link
        # GHC itself statically, but it seems to break haddock.
        # See https://github.com/input-output-hk/haskell.nix/issues/914#issuecomment-897424135
        linker-workaround = pkgsStatic: pkgs.writeShellScript "linker-workaround" ''
          # put all flags into 'params' array
          source ${pkgsStatic.stdenv.cc}/nix-support/utils.bash
          expandResponseParams "$@"

          # check if '-shared' flag is present
          hasShared=0
          for param in "''${params[@]}"; do
            if [[ "$param" == "-shared" ]]; then
              hasShared=1
            fi
          done

          if [[ "$hasShared" -eq 0 ]]; then
            # if '-shared' is not set, don't modify the params
            newParams=( "''${params[@]}" )
          else
            # if '-shared' is present, remove '-static' flag
            newParams=()
            for param in "''${params[@]}"; do
              if [[ ("$param" != "-static") ]]; then
                newParams+=( "$param" )
              fi
            done
          fi

          # invoke the actual linker with the new params
          exec x86_64-unknown-linux-musl-cc @<(printf "%q\n" "''${newParams[@]}")
        '';
      };
    });
}
