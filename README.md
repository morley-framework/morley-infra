<!--
SPDX-FileCopyrightText: 2022 Oxhead Alpha

SPDX-License-Identifier: LicenseRef-MIT-OA
-->

# Morley Infra

This repository provides a set of utilites that are used by CI in various projects
that are based on [Morley project](https://gitlab.com/morley-framework/morley).

These utilities are provided in the [`default.nix`](./default.nix).

## Usage instructions

Add this repository as `flake` input:
``` nix
{
  inputs = {
    ...
    morley-infra.url = "gitlab:morley-framework/morley-infra";
    ...
  };
  ...
}
```

To update locked inputs at the root of repository, run:
``` sh
nix shell "gitlab:morley-framework/morley-infra"#update-input -c update-input <commit-hash>
```

You can also add this repository as a dependency using `niv`:
``` sh
niv add morley-infra --rev <commit-hash> --template "https://gitlab.com/morley-framework/morley-infra/-/archive/<rev>/morley-infra-<rev>.tar.gz"
```

To update used `morley-infra` revision with `niv`, run:
``` sh
niv update morley-infra --rev <commit-hash>
```

## For Contributors

Please see [CONTRIBUTING.md](CONTRIBUTING.md).
